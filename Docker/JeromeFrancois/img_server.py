import socket
import threading
import base64
import os
import struct
import time
from PIL import ImageFilter
from PIL import Image
Image.MAX_IMAGE_PIXELS = None
from io import BytesIO

#Default value for server configuration
IP_addr = "localhost"
lstn_port = 9786
buff_size = 2048
separator = "::"
chunk_size = 2048
album_directory = "albums"


#Size in bytes for an integer value
int_size = 4

  
def rcv_int(client):
    """
    Read an int value encoded as four bytes in big endian

    Parameters:
        client (socket): socket to read

    Return:
        int: value read
    """

    #Read data from socket until the 4 bytes (int_size) has been read
    data = bytearray()
    while len(data)<int_size:
        data.extend(client.recv(min(int_size,int_size-len(data))))
    return struct.unpack('!i', data)[0]

def send_img_chunk(f,conn,lock):
    """
    Split a file as chunk and send the chunk (bytes) 

    parameters:
        f: file to send
        conn: socket to use
        lock: lock to ensure that all data are sent in a row 
    """
    filename = f.name.encode("UTF-8")
    s_filename = struct.pack('!i', len(filename))
    file = open(f, "rb")
    start = 0
    while True:
        read_bytes = file.read(chunk_size)
        n_read = len(read_bytes)
        if n_read>0:
            """
                To be implemented
            """
        else:
            break
        
    
def send_img(f,conn,lock):
    """
    Send a file as bytes to a socket with the length as 4-bytes header

    parameters:
        f: file to send
        conn: socket to use
        lock: lock to ensure that all data are sent in a row (length + payload)
    """
    data = open(f, "rb").read()
    val = struct.pack('!i', len(data))
    lock.acquire()
    conn.send(val) 
    conn.send(data) 
    lock.release()




def handle_client(conn, addr):
    """
        Function to handle a new connected client

        Parameters:
            conn (socket): socket used for the client
            addr (str): address of the client
    """

    #Add artifical delay for exercie purpose)
    #time.sleep(0.01)
    print(f"[NEW CONNECTION] {addr} connected.")
    
    all_data = bytearray()
    try:

        #Get the length of data sent by teh client
        expected_length = rcv_int(conn)
        
        #struct.unpack('!i', conn.recv(int_size))[0]

        #Read data until all expected data is read
        while len(all_data)<expected_length:
            data = conn.recv(buff_size)
            if data:
                all_data.extend(data)        
            else:
                print ('no more data from', addr)
                break
    except socket.error as e: 
            print ("Socket error: %s" %str(e)) 
    except Exception as e: 
        print ("Other exception: %s" %str(e))  

    #Decode the request from the client
    all_data = all_data.decode("UTF-8")

    #Extract the command and parameter based on the separator
    for li in all_data.splitlines():
        elements = li.split(separator)
        n_elements = len(elements)
        
        reply = ""
        #print(elements)
        try:
            #Handle the request based on the command provided 
            if n_elements>0:

                if elements[0] == "LIST":
                    #Get the albums and send it to the clients
                    for f in [ f.name for f in os.scandir(album_directory) if f.is_dir() ]:
                        reply = reply + "::"+str(f)
                    conn.send(reply.encode("UTF-8")) 
                    conn.close()
                    
                elif elements[0] == "ALBUM":
                    #Get the images of an album and send them as filename (we sort images by filename)
                    if n_elements ==2:
                        for f in sorted([ f.name for f in os.scandir(album_directory+"/"+elements[1]) if f.is_file()]):
                            reply = reply + "::"+str(f) 
                    conn.send(reply.encode("UTF-8")) 
                    conn.close()          

                elif elements[0] == "IMG":
                    #Load the image requested by the client and sent it back to it
                    if n_elements ==3:
                        img = open(album_directory+"/"+elements[1]+"/"+elements[2], "rb")
                        reply = reply + "::" + base64.b64encode(img.read()).decode("UTF-8")    
                        
                        conn.send(reply.encode("UTF-8")) 
                        conn.close()

                elif elements[0] == "IMGS":
                    if n_elements ==2:
                        
                        #On récupère la liste des images de l'album par ordre alphabétique
                        for f in sorted([f for f in os.scandir(album_directory+"/"+elements[1]) if f.is_file()],key=lambda x:x.name):
                            """
                                To be implemented
                            """ 
                        conn.send(reply.encode("UTF-8")) 
                        conn.close()


                
                elif elements[0] == "IMG_TRANSFORM":
                    #Load the image requested by the client and sent it back to it
                    if n_elements ==3:
                        fname = album_directory+"/"+elements[1]+"/"+elements[2]
                      
                        s=BytesIO()
                        img = open(fname, "rb")
                        img_transform = Image.open(img).resize([400,400]).filter(ImageFilter.GaussianBlur(10))
                        img_transform.save(s,format="JPEG")
                        
                        #img_transform.save(img_cache,format="JPEG")
                        reply = reply + "::" + base64.b64encode(s.getvalue()).decode("UTF-8")    

                        conn.send(reply.encode("UTF-8")) 
                        conn.close()
            else:
                print("Error")
        except socket.error as e: 
            print ("Socket error: %s" %str(e)) 
        finally:
            continue


def start_server():
    """
        Start the server and listen for clients
        For each new client connecting to the server, a dedicated thread will be dedicated to handle the connection
        The server will run until sig-int signal (Ctrl-C    )
    """
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR, 1)


    # Bind the socket to the port
    sock.bind((IP_addr,lstn_port))
    sock.listen(1)

    #Listen incoming connection
    while True:
        # Wait for a connection
        connection, client_address = sock.accept()
        try:   
            thread = threading.Thread(target=handle_client, args=(connection, client_address))
            thread.start()
        finally:
            continue



start_server()