
import struct
import threading
import time
import queue
import socket



def disp_msg():
      time.sleep(1)
      print("### Hello from the new thread")

def disp_msg_arg(name):
      time.sleep(1)
      print("### Hello from thread",name)

def disp_msg_lock(name,lock):
      lock.acquire()
      print("### Hello from the blocking thread",name)
      time.sleep(2)
      lock.release()

def tell_secret(queue):
      queue.put("OhOhOh")

def open_server():
      #Serveur TCP sur la boucle locale et le port 5555
      sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR, 1)
      sock.bind(("localhost",5555))
      sock.listen()

      print("### Server waiting its client")
      connection, client_address = sock.accept()

      msg = connection.recv(2048).decode("UTF-8")
      print("### Server getting message from the client - recv function", msg)
      print("### Server echoing back the message with some modifications - sendall function")
      reply = msg+" OhOhOh"
      connection.sendall(reply.encode("UTF-8"))
      print("### Server closing connection - close function")
      connection.close()

      print("### Server stopping and not accepting any other client - close function")
      sock.close()


#Rappel sur les encodages

print("\033[92m************** Encodage UTF-8 **************\033[0m")
print("La valeur d'une variable est représentée sous un format interne à python.\
Lorsque celle-ci doit être transmise ou stockée, on doit alors\
faire en sorte qu'elle soit encodée sous forme d'octets sous un format commun entre l'émetteur et le récepteur")

my_str = "ABCD"
print("Chaîne de caratcère en python --> ",my_str)
print("Chaîne de caractère binaire (bytes) encodée sour format UTF8 --> ",my_str.encode("UTF-8"))
print("Chaîne de caractère binaire (bytes) encodée sous format UTF-8 affichée sous forme d'une liste d'octets --> ",list((hex(x) for x in list(my_str.encode("UTF-8")))))
print("Décodage d'une chaîne de caractères encodée en UTF8 [0x45,0x46,0x47,0x48] --> ",(bytes([0x45,0x46,0x47,0x48]).decode("UTF-8")))
print("\n")

print("\033[92m************** Encodage (la suite) **************\033[0m")
print("Hormis les chaînes de caractères, nous devons également encoder les autres types de valeurs comme par exemple les entiers")
print("Les entiers sont codés sur 32 bits mais vous avez sûrement déjà entendu parler de l'ordre des octets qui peut différer selon l'encodage des systèmes")

my_int = 48621
print("Entier en python",my_int)
print("Entier en codage big endian struct.pack('!i', my_int) -- > ",struct.pack('!i', my_int))
print("Et on decode avec struct.unpack('!i', encoded_int)[0] -- > ",struct.unpack('!i', struct.pack('!i', my_int))[0])
print("Et si on décode avec le mauvais encodage, par exemple en little endian avec struct.unpack('i', encoded_int)[0] OOOpps !!!-- > ",struct.unpack('!i', struct.pack('i', my_int))[0])
print("\n")



#Rappel sur la manipulation d'octets

print("\033[92m************** Manipulation d'octets **************\033[0m")
print("Python permet de manipuler des données sous forme d'octets de plusieurs façons")
print("bytes est le type de base. Il n'est pas mutable et un objet de ce type ne peut donc pas être modifié")
print("Création d'un objet bytes de 5 octets tous égal 0xFF bytes([255,255,255,255,255]) --> ",bytes([255,255,255,255,255]))

print("Pour pouvoir manipuler et modifier un objet représentant des octets, on utilisera le type bytearray")
print("Conversion de 5 octets précédents en ByteArray, bytearray(bytes([255,255,255,255,255]) -->",bytearray(bytes([255,255,255,255,255])))

my_obj = bytearray(bytes([255,255,255,255,255]))

print("L'avantage de ce type est tout simplement de représenter les octets sous forme d'un tableau/liste")
print("On peut donc utiliser les opérations classiques sur les listes pour le modifier")

my_obj[0]=0x41
print("Modification du premier octet my_obj[0]=0x41 (A) -->",my_obj)
print("Avec la fonction print utilisé, python décode les caractères lorsqu'ils sont affichables comme le A")
print("Mais on peut afficher le code hexadécimal",list((hex(x) for x in my_obj)))

my_obj.extend("ABCD".encode("UTF-8"))
print("Ajout de la chaîne caractère ABCD encodée en UTF-8 -->",my_obj)
print("\n")


#Rappel sur les threads
print("\033[92m************** Threading en python **************\033[0m")
print("Le module threading permet d'exécuter une fonction dans un nouveau processus fils (multi-processing)")
print("Pour comprendre, examinez le code d'exemple de ce script")
print("1er exemple: creation et exécution d'un thread pour afficher un message après une seconde d'attente alors que le processus initial affiche aussi un message dès que possible")

new_thread = threading.Thread(target = disp_msg, args = [])
new_thread.start()
print("### Hello from main process")

print("Avant de continuer, atendons un peu la fin des threads time.sleep(2)")
time.sleep(2)

print("\n2eme exemple: grâce à la fonction join on peut synchroniser les threads... et on peut aussi passer dans args des arguments")
new_thread1 = threading.Thread(target = disp_msg_arg, args = ["First"])
new_thread2 = threading.Thread(target = disp_msg_arg, args = ["Second"])
new_thread1.start()
new_thread1.join()
new_thread2.start()
new_thread2.join()
print("### Hello from main process")
print("\n")

print("\033[92m************** Verrous **************\033[0m")

print("Il arrive souvent que les threads veulent accéder aux mêmes données ou objets")
print("Pour éviter des incohérences ou des erreurs d'IO, un mécanisme simple de verrou exclusif peut être utilisé")

print("On crée un verrou threading.Lock()")
lock = threading.Lock()
print("On prend le verrou (dès que disponible), lock.acquire()")
lock.acquire()
print("Et on n'oublie pas de déverouiller (ou rendre le verrou) une fois qu'on en a plus besoin\
car sinon on bloque les autres processus qui souhaiteraient l'utiliser")
lock.release()

print("Et en pratique, on peut passer ce verrou en paramètres des threads ")
new_thread1 = threading.Thread(target = disp_msg_lock, args = ["First",lock])
new_thread2 = threading.Thread(target = disp_msg_lock, args = ["Second",lock])
new_thread1.start()
new_thread2.start()
print("### Main process waits a bit")
time.sleep(0.5)
lock.acquire()
print("### Main process finally got the lock")
lock.release()
print("\n")

#Utilisation des files
print("\033[92m************** Files **************\033[0m")

print("L'utilisation de files est un moyen simple de communication entre plusieurs threads ou process (sur Linux un Thread est en fait instancié\
sous forme d'un process léger mais ce n'est pas forcément le cas avec d'autres OS)")
print("Creation d'une file queue.Queue()")
callback_queue = queue.Queue()

print("Un thread est créé et communiquera son secret au processus parent via la queue en utilisant les fonctions put et get")
new_thread = threading.Thread(target = tell_secret, args = [callback_queue])
new_thread.start()

print("### Main process waits for the secret callback_queue.get(block=True))",callback_queue.get(block=True))

#Les sockets

print("\033[92m************** Sockets **************\033[0m")

print("Et les sockets dans tout ça")
print("Voici un aperçu rapide mais vous aurez l'occasion d'expérimenter plus en détail au cours du TP")
print("Création d'une socket TCP en attente de connexion sur la machine locale et le port 5555 dans un thread à part")

new_thread = threading.Thread(target = open_server, args = [])
new_thread.start()

print("On attend un peu pour que le serveur démarre et on envoie un message 'Coucou'")
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(("localhost",5555))
client.sendall("Coucou".encode("UTF-8"))
print("On récupère la réponse du serveur dans un buffer de taille 2048 au maximum", client.recv(2048).decode("UTF-8"))
print("Fermeture de la connexion - close function")
client.close()

new_thread.join()
print("\n\033[93mC'était un exemple ultra-simplifié. En pratique il faut gérer les erreurs possibles (par exemple en cas de connexion interomppue) ou encore une transmission\
des données jusqu'à l'applicatif qui peut être découpée (la réponse complète n'est peut être pas encore dans le bufffer).\
Vous allez voir des exemples un peu plus complet dans le code fourni du client et serveur du TP\033[0m")

