import socket

import socket
import struct
from io import BytesIO

#Default configuration parameters for the client
srv_addr = "localhost"
srv_port = 9786
buff_size=1024
int_size = 4


#Size in bytes for an integer value
int_size = 4
separator="::"


def send_rcv(data):
    """
    Stateless function creating a new socket and sending data
    The function encodes the request as follows: data length (4 bytes) | data (* bytes) 
    Parameters
        data (str): data to be sent

    Returns:
        Bytes sent back by the remote address
    """

    all_data = ""
    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((srv_addr,srv_port))

        #Encode size as big endian
        val = struct.pack('!i', len(data))
        client.send(val)
        client.sendall(data.encode("UTF-8"))

        #Read bytes sent back until the socket is closed by the remote connection
        while True:
            data = client.recv(buff_size)
            if data:
                all_data += data.decode("UTF-8")
            else:
                print ('no more data from', srv_addr)
                break
    except socket.error as e: 
        print("Socket error: %s" %str(e)) 
    except Exception as e: 
        print("Other exception: %s" %str(e)) 
    finally:
        #Close socket from the client side
        client.close() 
        return all_data 

  
def rcv_int(client):
    """
    Read an int value encoded as four bytes in big endian

    Parameters:
        client (socket): socket to read

    Return:
        int: value read
    """

    #Read data from socket until the 4 bytes (int_size) has been read
    data = bytearray()
    while len(data)<int_size:
        data.extend(client.recv(min(int_size,int_size-len(data))))
    return struct.unpack('!i', data)[0]




        
def send_rcv_multi_chunks(data,gallery,callback):
    file_dict = {}
    try:
        #Step 1) get metadata about files and chunk
        meta_data = send_rcv("Step1_"+data)

        #We assume the following format: FILENAME1::SIZE_FILENAME1::FILENAME2::SIZE_FILENAME2
        elements = meta_data.split(separator)

        i=1
        while i < len(elements):
            fname = elements[i]
            size = int(elements[i+1])
            #initialization of file information
            
            file_dict[fname] = {}
            file_dict[fname]["size"] = size
            file_dict[fname]["bytes"] = bytearray(size)
            file_dict[fname]["bytes_read"] = 0
            i+=2

        
        #Step 2) get chuncks
        request = "Step2_"+data
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((srv_addr,srv_port))

        val = struct.pack('!i', len(request))
        client.send(val)
        client.sendall(request.encode("UTF-8"))
        i = 0
        while True:
            #all_data = bytes()
            
            #Pas très propre il faudrait fermer correctement la socket quand même...
           

            length_filename = rcv_int(client)
            
            if length_filename==0:
                client.close()
                break   

            total_read = 0
            all_data = bytearray()
            while total_read<length_filename:
                read_size = min(buff_size,length_filename-total_read)
                data = client.recv(read_size)
                total_read+=len(data)
                
                if data:
                    all_data.extend(data)# += data.decode("UTF-8")
                else:
                    print ('no more data')
                    break

            
            filename = all_data.decode("UTF-8")
            
            
            start_index = rcv_int(client)
  
            stop_index = rcv_int(client)
            
            length_to_read = stop_index - start_index
            total_read = 0

            all_data = bytearray()
            while total_read<length_to_read:
                read_size = min(buff_size,length_to_read-total_read)
                data = client.recv(read_size)
                total_read+=len(data)
                
                if data:
                    all_data.extend(data)
                else:
                    print ('no more data')
                    break
            #Save the number of byte read for the received chunk
            file_dict[filename]["bytes_read"]+=total_read
            #Insert byte at the right place into the bytearray of the current file
            
            bytes = file_dict[filename]["bytes"]
            for i in range(start_index,stop_index):
                bytes[i] = all_data[i-start_index]

            #If the file is complete, display the picture
            if file_dict[filename]["bytes_read"] == file_dict[filename]["size"]:
                callback(gallery,BytesIO(file_dict[filename]["bytes"]))

    except socket.error as e: 
        raise e
        print ("Socket error: %s" %str(e)) 
    # except Exception as e:
        
    #     for k,v in file_dict.items():
    #         print(k,v["size"],v["bytes_read"])

          
            
def send_rcv_multi(data,gallery,callback):
    
    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((srv_addr,srv_port))

        val = struct.pack('!i', len(data))
        client.send(val)
        client.sendall(data.encode("UTF-8"))

        while True:
            all_data = bytes()
            expected_length = rcv_int(client)
            total_read = 0
            print(expected_length)
        
            while total_read<expected_length:
                read_size = min(buff_size,expected_length-total_read)
                data = client.recv(read_size)
                total_read+=len(data)
                print("-->",total_read)
                if data:
                    all_data += data        
                else:
                    print ('no more data from', srv_addr)
                    break
            
            
            callback(gallery,BytesIO(all_data))
           

    except socket.error as e: 
        print ("Socket error: %s" %str(e)) 
        pass
    except Exception as e: 
        print ("Other exception: %s" %str(e)) 
    finally:
        client.close()


