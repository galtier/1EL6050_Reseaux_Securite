import streamlit as st
import img_client
import threading
from io import BytesIO
import base64
import time
import queue

separator = "::"

def list_album():
    """
    Get the list of albmus in directory albums

    Returns:
        The list of albums
    """

    reply = img_client.send_rcv("LIST")
    elements = reply.split(separator)
    if len(elements[0])>0:
        raise Exception("Reply must contain an empty first argument: "+str(elements[0]))
    else:
        return elements[1:]



def add_image_in_gallery(gallery,img):
    """
    Add an image to the gallery
    
    Parameters:
        gallery (dict): the gallery to update
        img (BytesIO): the image 
    """

    if gallery["current_col"]>=gallery["max_item_per_row"]:
        #reset col index and update row index
        gallery["current_row"] = gallery["top_container"].columns(gallery["max_item_per_row"])
        
        gallery["current_col"]=0


    st.session_state.gallery.append(gallery["current_row"][gallery["current_col"]].image(img))
    gallery["current_col"]+=1


def load_images(album,gallery):
    """
    Load the images of an album into the gallery
    
    Parameters;
        album (str): name of the album (directory name)
        gallery (dict): the gallery where images will be loaded
    """

    #Get image filenamaes of the albmus to be loaded
    cmd = "ALBUM"+separator+album
    reply = img_client.send_rcv(cmd)
    elements = reply.split(separator)

    if len(elements[0])>1:
        raise Exception("Reply must contain an empty first argument: "+str(elements[0]))
    else:
        #Iterate over every single image, load it and it to the gallery    
        for e in elements[1:]:
            cmd = "IMG"+separator+album+separator+e
            reply = img_client.send_rcv(cmd)
            elements2 = reply.split(separator)
            
            if len(elements2[0])>1:
                raise Exception("Reply must contain an empty first argument: "+str(elements2[0]))
            elif len(elements2)<2:
                raise Exception("No image returned"+str(e))
            else:
                im = BytesIO(base64.b64decode(elements2[1]))
                add_image_in_gallery(gallery,im)

def load_transform_images(album,gallery):
    """
    Similar as load_images butwith TRANSFORM command
    
    Parameters;
        album (str): name of the album (directory name)
        gallery (dict): the gallery where images will be loaded
    """

    #Get image filenamaes of the albmus to be loaded
    cmd = "ALBUM"+separator+album
    reply = img_client.send_rcv(cmd)
    elements = reply.split(separator)

    if len(elements[0])>1:
        raise Exception("Reply must contain an empty first argument: "+str(elements[0]))
    else:
        #Iterate over every single image, load it and it to the gallery    
        for e in elements[1:]:
            cmd = "IMG_TRANSFORM"+separator+album+separator+e
            reply = img_client.send_rcv(cmd)
            elements2 = reply.split(separator)
            
            if len(elements2[0])>1:
                raise Exception("Reply must contain an empty first argument: "+str(elements2[0]))
            elif len(elements2)<2:
                raise Exception("No image returned"+str(e))
            else:
                im = BytesIO(base64.b64decode(elements2[1]))
                add_image_in_gallery(gallery,im)



def load_images_optim1(album_dropdown,gallery):
    
    #Get image filenames
    cmd = "ALBUM"+separator+album_dropdown   
    reply = img_client.send_rcv(cmd)
    elements = reply.split(separator)
    if len(elements[0])>1:
        raise Exception("Reply must contain an empty first argument: "+str(elements[0]))
    else:
     
        cmd = "IMGS"+separator+album_dropdown
             
        reply = img_client.send_rcv(cmd)
        elements2 = reply.split(separator)
    
        
        if len(elements2[0])>1:
            raise Exception("Reply must contain an empty first argument: "+str(elements[0]))
        elif len(elements2)<2:
            raise Exception("No image returned")
        else:
            """
            To be implemented
            """



def load_images_optim2(album_dropdown,gallery):
    
    #Get image filenames
    cmd = "ALBUM"+separator+album_dropdown
    
    reply = img_client.send_rcv(cmd)
    elements = reply.split(separator)
    if len(elements[0])>1:
        raise Exception("Reply must contain an empty first argument: "+str(elements[0]))
    else:
        callback_queue = queue.Queue()
    """
    To be implemented
    """
            

def load_images_optim3(album_dropdown,gallery):
    """
        To be implemented
    """
    
    
def load_images_optim4(album_dropdown,gallery):
    """
        To be implemented
    """  

#Creation of the application

st.title("Galerie")

#State variable to keep track of the status of the gui session
if 'find' not in st.session_state:
   st.session_state.find = False
    
if st.button("Find albums",key="find_albums"):
    st.session_state.list_albums = list_album()
    st.session_state.find = True

 
option_album = st.empty()
if st.session_state.find:
    option_album=st.selectbox("Select an album",st.session_state.list_albums)
else:
    option_album=st.selectbox("Select an album",[])

col_buttons = st.columns(6)

click_v1 =  col_buttons[0].button("Load Images (v1)",key="v1")
click_v2 =  col_buttons[1].button("Load Images (v2)",key="v2")
click_v3 =  col_buttons[2].button("Load Images (v3)",key="v3")
click_v4 =  col_buttons[3].button("Load Images (v4)",key="v4")
click_v5 =  col_buttons[4].button("Load Images (v5)",key="v5")
click_v6 =  col_buttons[5].button("Transform Images",key="v6")

top = st.container()

if 'gallery' in st.session_state:
   for e in st.session_state.gallery:
       e.empty()
   top.empty()
   time.sleep(0.01)
   
timer = top.container()

#Creation of the initial gallery (empty. Thi is represented in column format
gallery = {}
gallery["current_col"] = 0
gallery["max_item_per_row"] = 8
gallery["top_container"] = top
gallery["current_row"] = gallery["top_container"].columns(gallery["max_item_per_row"])
st.session_state.gallery = []

t_start = time.time()

if click_v1:
    load_images(option_album,gallery)
elif click_v2:
    load_images_optim1(option_album,gallery)
elif click_v3:
    load_images_optim2(option_album,gallery)
elif click_v4:
    load_images_optim3(option_album,gallery)
elif click_v5:
    load_images_optim4(option_album,gallery)
elif click_v6:
    load_transform_images(option_album,gallery)

timer.write("Temps écoulé (secondes): "+str(time.time() - t_start)) 